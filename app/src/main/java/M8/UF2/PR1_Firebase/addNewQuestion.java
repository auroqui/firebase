package M8.UF2.PR1_Firebase;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class addNewQuestion extends AppCompatActivity {


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mQuestionsDatabaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_question);
        Button retrn = findViewById(R.id.addToMain);
        Button submit = findViewById(R.id.submit);

        final EditText preguntaTxt = findViewById(R.id.pregunta);
        final EditText respuestaTxt = findViewById(R.id.respuesta);

        final EditText dummy1 = findViewById(R.id.respostaDummy1);
        final EditText dummy2 = findViewById(R.id.respostaDummy2);
        final EditText dummy3 = findViewById(R.id.respostaDummy3);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mQuestionsDatabaseReference = mFirebaseDatabase.getReference().child("preguntas");


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!preguntaTxt.getText().toString().isEmpty() || !respuestaTxt.getText().toString().isEmpty() ||
                        !dummy1.getText().toString().isEmpty() || !dummy2.getText().toString().isEmpty() || !dummy3.getText().toString().isEmpty()) {
                    pregunta pregunta = new pregunta(preguntaTxt.getText().toString(), respuestaTxt.getText().toString(), dummy1.getText().toString(), dummy2.getText().toString(), dummy3.getText().toString());

                    mQuestionsDatabaseReference.push().setValue(pregunta);


                    preguntaTxt.setText("");
                    respuestaTxt.setText("");
                    dummy1.setText("");
                    dummy2.setText("");
                    dummy3.setText("");
                } else {
                    Toast.makeText(addNewQuestion.this, "Inserta tots els valors", Toast.LENGTH_SHORT).show();
                }
            }
        });

        retrn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });


    }

}
