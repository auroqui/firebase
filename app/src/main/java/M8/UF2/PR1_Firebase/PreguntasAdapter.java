package M8.UF2.PR1_Firebase;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

public class PreguntasAdapter extends RecyclerView.Adapter<PreguntasAdapter.ViewHolder> {

    private int resource;
    private ArrayList<pregunta> preguntasList;

    public PreguntasAdapter(ArrayList<pregunta> preguntasList, int resource) {
        this.preguntasList = preguntasList;
        this.resource = resource;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        pregunta pregun = preguntasList.get(position);

        holder.txtPregunta.setText(pregun.getPregunta());
        holder.txtRespuesta.setText(pregun.getResposta());
        holder.txtDummy1.setText(pregun.getDummy1());
        holder.txtDummy2.setText(pregun.getDummy2());
        holder.txtDummy3.setText(pregun.getDummy3());


    }

    @Override
    public int getItemCount() {
        return preguntasList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        private TextView txtPregunta;
        private TextView txtRespuesta;
        private TextView txtDummy1;
        private TextView txtDummy2;
        private TextView txtDummy3;

        public ViewHolder(View view) {
            super(view);

            this.view = view;
            this.txtPregunta = (TextView) view.findViewById(R.id.preguntaR);
            this.txtRespuesta = (TextView) view.findViewById(R.id.respuestaR);
            this.txtDummy1 = (TextView) view.findViewById(R.id.dummy1R);
            this.txtDummy2 = (TextView) view.findViewById(R.id.dummy2R);
            this.txtDummy3 = (TextView) view.findViewById(R.id.dummy3R);
        }

    }


}
