package M8.UF2.PR1_Firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class viewQuestions extends AppCompatActivity {


    private FirebaseDatabase mFirebaseDatabase;

    private PreguntasAdapter pAdapter;
    private RecyclerView refRview;
    private ArrayList<pregunta> prList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_questions);
        Button retrn = findViewById(R.id.viewToMain);
        Button run = findViewById(R.id.refreshButton);
        refRview = findViewById(R.id.recyclerViewQuestions);


        refRview.setLayoutManager(new LinearLayoutManager(this));

        mFirebaseDatabase = FirebaseDatabase.getInstance();


        run.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPreguntesFromFirebase();
            }
        });


        retrn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getPreguntesFromFirebase() {
        mFirebaseDatabase.getReference().child("preguntas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    prList.clear();

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        String pregunta = ds.child("pregunta").getValue().toString();
                        String respuesta = ds.child("resposta").getValue().toString();
                        String dummy1 = ds.child("dummy1").getValue().toString();
                        String dummy2 = ds.child("dummy2").getValue().toString();
                        String dummy3 = ds.child("dummy3").getValue().toString();

                        prList.add(new pregunta(pregunta, respuesta, dummy1, dummy2, dummy3));
                    }

                    pAdapter = new PreguntasAdapter(prList, R.layout.pregunta_view);
                    refRview.setAdapter(pAdapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
