package M8.UF2.PR1_Firebase;

import com.google.firebase.firestore.IgnoreExtraProperties;

@IgnoreExtraProperties
public class pregunta {

    String pregunta;
    String resposta;
    String dummy1;
    String dummy2;
    String dummy3;

    public pregunta() {

    }

    public pregunta(String pregunta, String resposta, String dummy1, String dummy2, String dummy3) {
        this.pregunta = pregunta;
        this.resposta = resposta;
        this.dummy1 = dummy1;
        this.dummy2 = dummy2;
        this.dummy3 = dummy3;
    }

    public String getDummy1() {
        return dummy1;
    }

    public void setDummy1(String dummy1) {
        this.dummy1 = dummy1;
    }

    public String getDummy2() {
        return dummy2;
    }

    public void setDummy2(String dummy2) {
        this.dummy2 = dummy2;
    }

    public String getDummy3() {
        return dummy3;
    }

    public void setDummy3(String dummy3) {
        this.dummy3 = dummy3;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
}
