package M8.UF2.PR1_Firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Answer extends AppCompatActivity implements OnMapReadyCallback {


    private FirebaseDatabase mFirebaseDatabase;
    private TextView preguntaText;
    private PreguntasAdapter pAdapter;
    private ArrayList<pregunta> prList = new ArrayList<>();
    private int ctrl = 0;
    private EditText resposta;
    private TextView dummy1;
    private TextView dummy2;
    private TextView dummy3;
    private TextView correcta;

    private GoogleMap mMap;


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in madrid, Spain, and move the camera.
        LatLng madrid = new LatLng(40.3, -3.7);
        mMap.addMarker(new MarkerOptions().position(madrid).title("Marker in madrid"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(madrid));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
        Button retrn = findViewById(R.id.answerToMain);
        Button nextQst = findViewById(R.id.buttonNextQUestion);
        Button send = findViewById(R.id.submitAnswer);
        preguntaText = findViewById(R.id.questionToAnswer);
        resposta = findViewById(R.id.insertResposta);
        dummy1 = findViewById(R.id.dummyRView1);
        dummy2 = findViewById(R.id.dummyRView2);
        dummy3 = findViewById(R.id.dummyRView3);
        correcta = findViewById(R.id.correcta);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        getPreguntesFromFirebase();

        nextQst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (prList.size() <= ctrl) {
                    ctrl = 0;
                    preguntaText.setText(prList.get(ctrl).getPregunta());
                    correcta.setText(prList.get(ctrl).getResposta());
                    dummy1.setText(prList.get(ctrl).getDummy1());
                    dummy2.setText(prList.get(ctrl).getDummy2());
                    dummy3.setText(prList.get(ctrl).getDummy3());
                } else {
                    preguntaText.setText(prList.get(ctrl).getPregunta());
                    correcta.setText(prList.get(ctrl).getResposta());
                    dummy1.setText(prList.get(ctrl).getDummy1());
                    dummy2.setText(prList.get(ctrl).getDummy2());
                    dummy3.setText(prList.get(ctrl).getDummy3());
                }


            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                inserirResposta(prList.get(ctrl).getPregunta(), prList.get(ctrl).getResposta());

            }

        });


        retrn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });


    }


    private void inserirResposta(String preguntaFire, String respostaFire) {

        if (respostaFire.equalsIgnoreCase(resposta.getText().toString())) {
            Toast tst01 = Toast.makeText(getApplicationContext(),
                    "Correcte!", Toast.LENGTH_SHORT);
            tst01.show();
            ctrl++;
            if (prList.size() <= ctrl) {
                ctrl = 0;
                preguntaText.setText(prList.get(ctrl).getPregunta());
                correcta.setText(prList.get(ctrl).getResposta());
                dummy1.setText(prList.get(ctrl).getDummy1());
                dummy2.setText(prList.get(ctrl).getDummy2());
                dummy3.setText(prList.get(ctrl).getDummy3());

            } else {
                preguntaText.setText(prList.get(ctrl).getPregunta());
                correcta.setText(prList.get(ctrl).getResposta());
                dummy1.setText(prList.get(ctrl).getDummy1());
                dummy2.setText(prList.get(ctrl).getDummy2());
                dummy3.setText(prList.get(ctrl).getDummy3());
            }
        } else {
            Toast tst02 = Toast.makeText(getApplicationContext(),
                    "Incorrecte: " + respostaFire + " La teva: " + resposta.toString(), Toast.LENGTH_SHORT);
            tst02.show();
        }


    }


    private void getPreguntesFromFirebase() {
        mFirebaseDatabase.getReference().child("preguntas").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    prList.clear();

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        String pregunta = ds.child("pregunta").getValue().toString();
                        String respuesta = ds.child("resposta").getValue().toString();
                        String dummy1 = ds.child("dummy1").getValue().toString();
                        String dummy2 = ds.child("dummy2").getValue().toString();
                        String dummy3 = ds.child("dummy3").getValue().toString();


                        prList.add(new pregunta(pregunta, respuesta, dummy1, dummy2, dummy3));
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
